import React from 'react';
import { PreviewStyles } from '../styles/Preview';

export const Preview: React.FC = () => {
  return (
    <div style={PreviewStyles.container}>
      <h1>Preview</h1>
    </div>
  );
};
