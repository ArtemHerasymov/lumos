import React from 'react';
import { ToolbarStyles } from '../styles/Toolbar';

export const Toolbar: React.FC = () => {
  return (
    <div style={ToolbarStyles.container}>
      <h1>Toolbar</h1>
    </div>
  );
};
