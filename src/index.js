import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Container } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';
import outerTheme from './theme';

ReactDOM.render(
  (
    <ThemeProvider theme={outerTheme}>
      <Container maxWidth={false}>
        <App />
      </Container>
    </ThemeProvider>
  ),
  document.getElementById('root'),
);

serviceWorker.unregister();
