import React from 'react';
import './App.css';
import { Preview } from './components/Preview';
import { Toolbar } from './components/Toolbar';
import { Settings } from './components/Settings';

function App() {
  return (
    <div style={{ flex: 1, flexDirection: 'row', margin: 0, }}>
      <div style={{ flex: 1, flexDirection: 'row', }}>
        <div style={{ }}><Preview /></div>
        <div style={{ }}><Settings /></div>
      </div>
      <Toolbar />
    </div>
  );
}

export default App;
